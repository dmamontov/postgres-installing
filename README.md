# postgres-installing

запуск:

`docker-compose up`

корректное подключение к базе через dbeaver:
[ссылка](https://gitlab.com/dmamontov/postgres-installing/-/blob/master/connect_dbeaver.png)

подключение через psql:
```
[~/local/postgres-installing]$ psql -h localhost -p 5432 -d user_database -U user                                                                                                                                                                                    *[master]
Password for user user:
psql (12.4, server 13.1 (Debian 13.1-1.pgdg100+1))
WARNING: psql major version 12, server major version 13.
         Some psql features might not work.
Type "help" for help.

user_database=# 
```


создаём нового юзера
```
user_database=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 user      | Superuser, Create role, Create DB, Replication, Bypass RLS | {}

user_database=#
user_database=#
user_database=#
user_database=#
user_database=# CREATE USER new_user WITH PASSWORD 'secret';
CREATE ROLE
user_database=# GRANT ALL PRIVILEGES ON DATABASE "user_database" to new_user;
GRANT
user_database=#
user_database=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 new_user  |                                                            | {}
 user      | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
```


подключаемся из-под нового пользователя:
```
[~/local/postgres-installing]$ psql -h localhost -p 5432 -d user_database -U new_user                                                                                                                                                                                *[master]
Password for user new_user:
psql (12.4, server 13.1 (Debian 13.1-1.pgdg100+1))
WARNING: psql major version 12, server major version 13.
         Some psql features might not work.
Type "help" for help.

user_database=> \conninfo
You are connected to database "user_database" as user "new_user" on host "localhost" (address "::1") at port "5432".
user_database=>
```

